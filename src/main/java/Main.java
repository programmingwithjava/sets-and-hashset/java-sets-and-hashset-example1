import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
*
* sets are used less compared to lists and maps.
* set is an ordered set of items and can contain duplicates.
* sets have no ordering, and they cannot contain duplicates.
* there are also ordered set implementations like LinkedHashSet and TreeSet (map objects are used in the underlying implementation).
* if you wanna make sure there are no duplicates in your collection, then you could use a set rather than a list. this is the
* biggest differentiator of the Set interface compared to the other interfaces in the Collections framework.
* Set interface is also generic like other interfaces in the Collections framework and it takes a single type.
*
*
* best performing implementation of the Set interface is the HashSet class. it uses hashes to store the items.
* HashSet implementation uses a HashMap (Java 8). when a new element is added to the HashSet, it becomes a key in the underlying HashMap
* and a dummy object is stored as the value.
* Java Collections' Set type allow the usual set operations such as union and intersection.
* Sets can be useful because the operations on them are very fast.
*
* Set interfaces defines some basic methods such as add, remove and clear.
* size, isEmpty, contains are some of the other useful methods provided by the Set interface.
*
* there is no way to retrieve an item from a set like it can be done in a List. you can check if something exists, you can iterate over
* the elements but getting Nth element in a set is not possible (remember, there is no ordering).
*
* Keys in a Map can be retrieved as a set using keySet() method.
*
*
* It is preferred to use immutable objects as elements of a Set:
*
* Because these collections rely on hashing, fields that contribute to their hashCode should be immutable.
* When a HashMap wants to store a key-value, it uses hashCode of its key and works out a place for the pair,
* The same technique will be used for elements retrieval ( ex: contains,get, etc..). Now imagine hashCode upon element
* retrieval produces a value different than the one produced at the time the elements were added ? Would we be able to locate the element correctly? No.

* HashSet is no different from a HashMap.

* It's all about having a hashCode and equals methods that are able to compare objects correctly,
* immutability makes it easier to reason about the correctness of these methods.
*
* ref:https://stackoverflow.com/questions/37335407/why-is-it-preferred-to-use-immutable-objects-as-elements-of-a-set
* */

public class Main {
    
    private static Map<String, CelestialObject> solarSystem = new HashMap<>();
    private static Set<CelestialObject> planets = new HashSet<>();

    public static void main(String[] args) {

    	/*
    	 * below we are referencing "moon" from multiple Collection interface implementations (HashSet, HashMap) but there is 
    	 * only a single CelestialObject instance. don't forget that. 
    	 * for example, only references are stored in the sets and maps in below example.
    	 * */
    	
        CelestialObject temp = new CelestialObject("mercury", 88);
        solarSystem.put(temp.getName(), temp);
        planets.add(temp);

        temp = new CelestialObject("venus", 225);
        solarSystem.put(temp.getName(), temp);
        planets.add(temp);

        temp = new CelestialObject("earth", 365);
        solarSystem.put(temp.getName(), temp);
        planets.add(temp);

        CelestialObject tempMoon = new CelestialObject("moon", 27);
        solarSystem.put(tempMoon.getName(), tempMoon);
        temp.addMoon(tempMoon);

        System.out.println("Planets:");

        for(CelestialObject co : planets){
            System.out.println("\t" + co.getName());
        }
        
        CelestialObject obj = solarSystem.get("earth");
        System.out.println("Moons of " + obj.getName());
        for(CelestialObject moon : obj.getSatellites()) {
        	System.out.println("\t" + moon.getName());
        }
        /*
         * creating a Set union using addAll method
         * if an element exists in more than one sets, there will be only a single element inside the union since 
         * sets don't contain duplicates. each element in the union will appear only once if it was present in more than one of the sets. 
         * */
        Set<CelestialObject> moons = new HashSet<>();
        for(CelestialObject planet : planets) {
        	moons.addAll(planet.getSatellites());
        }
        
        System.out.println("All Moons:");
        for(CelestialObject moon : moons) {
        	System.out.println("\t" + moon.getName());
        }

        /*
        * we were able to add another venus to the set. but sets can include an element only once.
        * how does this happen? the reason is the 2 venus objects are not equal here.
        * this is why the equals method has to be overridden in the CelestialObject class.
        * all the classes in Java are based on Object class, and Object class implements a very simple equals method
        * which checks for referential equality. if both references point to the same object then they will be considered equal.
        * this is the default "equals" implementation that comes with the base "Object" class. which uses "==" operator
        * to check for referential equality.
        *
        * equals and hashCode:
        * equals and hashCode explained for HashSet and HashMap classes...
        *
        * when storing objects in a Hashed collection like HashSet and HashMap think about the collections as
        * a set of buckets to store the objects.
        * the hash code determines which bucket an object is gonna go into.
        * any objects that are equal should always have the same hash code (a requirement of the hashed collections not a
        * general requirement for implementing "equals" methods!) and they will end up in the same bucket.
        *
        * so when we add an object, its has code tells the collection which bucket it has to go in.
        * there may already be objects in that bucket so each is compared to the new object to make sure it is
        * not already in there. equals method will be used to check if the objects are actually equal.
        *
        * if we try to add an object A that is equal to an object B which is in bucket 1, hash code should indicate that
        * bucket 1 is the place to check for existing objects' equality.
        *
        * collection class will check all the objects in bucket 1, and once the equal object B is found, the new object A will
        * not be added to the collection.
        *
        * but if our new objects breaks the rules and generates a different hash code than object B, even though it compares equal,
        * collection will be looking at the wrong bucket and not be able to the existing same object. in this case we would end up
        * with a duplicate object in our hashed collection.
        *
        * this could result in further problem when we try to delete the recently added copy and end up deleteing the original object.
        *
        * ultimately there is a strict relationship between hashcode and equals methods for our hashed collection implementations
        * like HashSet and HashMap.
        *
        * result: if 2 objects compare equal, then they MUST have the same has code in case of hashed collections.
        * */
        CelestialObject venus = new CelestialObject("venus", 255);
        planets.add(venus);

        /*
        * there will be 2 outputs for venus in the loop below.
        * */
        for(CelestialObject planet : planets){
            System.out.println(planet.getName() + ":" + planet.getOrbitPeriod());
        }
    }
}
