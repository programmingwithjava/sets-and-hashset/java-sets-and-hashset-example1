import java.util.HashSet;
import java.util.Set;

public final class CelestialObject {
    private final String name;
    private final double orbitPeriod;
    private final Set<CelestialObject> satellites;

    /*
    * we need to override the equals and hashCode methods for our HashSet and HashMap collections (see main class)
    * to work properly.
    * see the comments in the main class why!
    * */

    /*
    * overloading a method instead of overriding is a mistake that can be made easily once you got the wrong method signature.
    * always use @Override annotation to avoid such kind of mistakes.
    *
    * pay attention that the type ob obj below should be "Object" not "CelestialObject".
    * */
    @Override
    public boolean equals (Object obj){
        if(this == obj){
            return true;
        }
        System.out.println("obj class is " + obj.getClass());
        System.out.println("this class is " + this.getClass());
        if(obj == null || (obj.getClass() != this.getClass())){
            return false;
        }
        return this.name.equals(((CelestialObject) obj).getName());
    }

    /*
    * since for the case of planets we are only considering names as unique identifiers, we can use String class's
    * hashCode method for the overridden implementation of Object class's hashCode.
    * to take the easy road you could have overridden the hashCode method in a way that it always returns a fixed number like 1.
    * in this case all the objects would be put in the same bucket, and every time a new object is added to the hashed collection
    * all the items in that same bucket would be compared to the items being added. this would greatly reduce the performance
    * and defeat the purpose of using hashed collections.
    *
    * now that we have overridden the equals and hashCode methods, there will be only 1 venus in the output of our program.
    * the 2nd venus won't be added.
    * */
    @Override
    public int hashCode() {
        System.out.println("hashCode invoked");
        return this.name.hashCode() + 31;
    }

    public CelestialObject(String name, double orbitPeriod) {
        this.name = name;
        this.orbitPeriod = orbitPeriod;
        this.satellites = new HashSet<CelestialObject>();
    }

    public String getName() {
        return name;
    }

    public double getOrbitPeriod() {
        return orbitPeriod;
    }

    /*
    * for a better encapsulation and for an immutable class instead of the default getter implementation provided
    * by IDEA IDE, we return a new HashSet instance. this way the returned HashSet will be a different set of references to the 
    * CelestialObject instances. hence, any changes made in this returned set will not change the original set which is "satellites".
    * */
    public Set<CelestialObject> getSatellites() {
        //return satellites;
        return new HashSet<CelestialObject>(this.satellites);
    }

    public boolean addMoon(CelestialObject moon){
        return this.satellites.add(moon);
    }
}
